#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#define PORT 4444

struct allowed{
	char name[10000];
	char password[10000];
};

struct allowed_database{
	char database[10000];
	char name[10000];
};

void createUser(char *nama, char *password){
	struct allowed user;
	strcpy(user.name, nama);
	strcpy(user.password, password);
	printf("%s %s\n", user.name, user.password);
	char fname[]={"databases/user.dat"};
	FILE *fp;
	fp=fopen(fname,"ab");
	fwrite(&user,sizeof(user),1,fp);
	fclose(fp);
}

int cekUserExist(char *username ){
	FILE *fp;
	struct allowed user;
	int id,found=0;
	fp=fopen("../database/databases/user.dat","rb");
	while(1){	
		fread(&user,sizeof(user),1,fp);
		// printf("%s %s\n", user.name, username);
		// printf("%s %s\n", user.password, password);
		// printf("masuk");
		if(strcmp(user.name, username)==0){
			return 1;
		}
		if(feof(fp)){
			break;
		}
	}
	fclose(fp);
	// printf("found %d: \n",found);
	return 0;
	
}

void insertPermission(char *nama, char *database){
	struct allowed_database user;
	strcpy(user.name, nama);
	strcpy(user.database, database);
	printf("%s %s\n", user.name, user.database);
	char fname[]={"databases/permission.dat"};
	FILE *fp;
	fp=fopen(fname,"ab");
	fwrite(&user,sizeof(user),1,fp);
	fclose(fp);
}

int cekAllowedDatabase(char *nama, char *database){
	FILE *fp;
	struct allowed_database user;
	int id,found=0;
	printf("nama = %s  database = %s", nama, database);
	fp=fopen("../database/databases/permission.dat","rb");
	while(1){	
		fread(&user,sizeof(user),1,fp);
		if(strcmp(user.name, nama)==0){
			// printf("MASUK!");
			if(strcmp(user.database, database)==0){
				return 1;
			}
		}
		if(feof(fp)){
			break;
		}
	}
	fclose(fp);
	return 0;
}

int main(int argc, char const *argv[]) {
  int sockfd, ret;
	 struct sockaddr_in serverAddr;

	int newSocket;
	struct sockaddr_in newAddr;

	socklen_t addr_size;

	char buffer[1024];
	pid_t childpid;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0){
		printf("[-]Error in connection.\n");
		exit(1);
	}
	printf("[+]Server Socket is created.\n");

	memset(&serverAddr, '\0', sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	ret = bind(sockfd, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
	if(ret < 0){
		printf("[-]Error in binding.\n");
		exit(1);
	}
	printf("[+]Bind to port %d\n", 4444);

	if(listen(sockfd, 10) == 0){
		printf("[+]Listening....\n");
	}else{
		printf("[-]Error in binding.\n");
	}


	while(1){
				recv(newSocket, buffer, 1024, 0);
				// printf("buffer %s\n", buffer);
				char *token;
				char buffercopy[32000];
				strcpy(buffercopy, buffer);
				char perintah[100][10000];
				token = strtok(buffercopy, ":");
				int i=0;
				char database_used[1000];
				while( token != NULL ) {
					strcpy(perintah[i], token);
					// printf("%s\n", perintah[i]);
					i++;
					token = strtok(NULL, ":");
				}
				if(strcmp(perintah[0], "cUser")==0){
					if(strcmp(perintah[3], "0")==0){
						createUser(perintah[1], perintah[2]);
					}else{
						char peringatan[] = "You're Not Allowed";
						send(newSocket, peringatan, strlen(peringatan), 0);
						bzero(buffer, sizeof(buffer));
					}
				}else if(strcmp(perintah[0], "gPermission")==0){
					if(strcmp(perintah[3], "0")==0){
						int exist = cekUserExist(perintah[2]);
						if(exist == 1){
							insertPermission(perintah[2], perintah[1]);
						}else{
							char peringatan[] = "User Not Found";
							send(newSocket, peringatan, strlen(peringatan), 0);
							bzero(buffer, sizeof(buffer));
						}
					}else{
						char peringatan[] = "You're Not Allowed";
						send(newSocket, peringatan, strlen(peringatan), 0);
						bzero(buffer, sizeof(buffer));
					}
				}else if(strcmp(perintah[0], "cDatabase")==0){
					char lokasi[20000];
					snprintf(lokasi, sizeof lokasi, "databases/%s", perintah[1]);
					printf("lokasi = %s, nama = %s , database = %s\n", lokasi, perintah[2], perintah[1]);
					mkdir(lokasi,0777);
					insertPermission(perintah[2], perintah[1]);
				}else if(strcmp(perintah[0], "uDatabase") == 0){		
					if(strcmp(perintah[3], "0") != 0){
						int allowed = cekAllowedDatabase(perintah[2], perintah[1]);
						if(allowed != 1){
							char peringatan[] = "Access_database : You're Not Allowed";
							send(newSocket, peringatan, strlen(peringatan), 0);
							bzero(buffer, sizeof(buffer));
						}else{
							strncpy(database_used, perintah[1], sizeof(perintah[1]));
							char peringatan[] = "Access_database : Allowed";
							printf("database_used = %s\n", database_used);
							send(newSocket, peringatan, strlen(peringatan), 0);
							bzero(buffer, sizeof(buffer));
						}
					}
				}
			}
}
